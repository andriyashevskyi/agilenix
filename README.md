This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.

You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

##INSTALL and Start

yarn install

yarn start

##Структура

./src/fetchCrypt.js - запрос для получения данных 

./src/sort/sort.js  - сортировка массива данных по порядку

./src/actions/ - экшн генераторы

./src/reducers/ - редьюсеры

./src/store/ - стор

./src/components/ - компоненты: строка таблицы и прелоадер

./src/containers/  - таблица