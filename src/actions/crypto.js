import { checkResponse, getPromise, sortArr } from '../fetchCrypt';
import { cryptoFetchFailed, cryptoFetchRequested, cryptoFetchSucceeded } from "./request";

export const addCrypto = (data) => ( {
  type: 'ADD_CRYPTO',
  data
} );


// Возможно в cryptoFetchFailed нужно передать еще и ошибку, чтоб вывести пользователю информацию об ошибке
//TODO передать ошибку

export const gettingCrypto = () => {
  return (dispatch) => {
    dispatch(cryptoFetchRequested());

    let cryptoPromise = getPromise
        .then(checkResponse)
        .then(sortArr)
        .catch(err => {
          console.log(err);
          return {type: 'error'};
        });

    cryptoPromise.then((data) => {
      if ( data.hasOwnProperty('type') ) {
        dispatch(cryptoFetchFailed());
      }
      else {
        dispatch(cryptoFetchSucceeded());
        dispatch(addCrypto(data));
      }

    })

  }
};