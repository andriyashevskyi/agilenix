import React, { Component } from 'react';
import PropTypes from 'prop-types';

function Preloader() {
  return <div className="preloader"><img
      src="https://loading.io/spinners/blocks/lg.rotating-squares-preloader-gif.gif"/></div>;
}


export default Preloader;