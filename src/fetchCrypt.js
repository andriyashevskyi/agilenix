import sortCryptWith from "./sort/sort";

export const getPromise = new Promise((resolve, reject) => {
  fetch('https://min-api.cryptocompare.com/data/all/coinlist')
      .then((response) => {
        if ( !response.ok ) {
          reject(response)
        }

        const body = response.json();
        resolve(body);

      })
});


/**
 * проверяем ответ от сервера
 * @param data
 * @returns {*}
 */
export const checkResponse = (data) => {
  const {Response: response, Message: msg, ErrorsSummary: errSum} = data;
  if ( response === 'Error' ) {
    throw  Error(`Oops, here bad request ${msg} ${errSum}`);
  }
  return ( data );
};
/***
 * сортируем полученный массив объектов
 * @param data
 * @returns {Array}
 */
export const sortArr = (data) => {
  let cryptoArr = [];
  const {BaseImageUrl: imgUrl, Data: coins,} = data;

  for ( let key in coins ) {
    if ( coins.hasOwnProperty(key) ) {
      //Запрос на получение цены можно засунуть вот сюда в таком виде, либо же сделать async/await
      //Но  сервер для некоторых значений будет отдавать 99 ошибку

      // fetch(`https://min-api.cryptocompare.com/data/price?fsym=${coins[ key ].Name}&tsyms=USD`)
      //     .then((data) => {
      //       return data.json()
      //     })
      //     .then((data) => {
      //       console.log(data);
      //       coins[ key ].price = data[ "USD" ];
      //     });
      coins[ key ].baseImgUrl = imgUrl;

      cryptoArr.push(coins[ key ]);
    }
  }
  cryptoArr.sort(sortCryptWith);
  return cryptoArr;
};


