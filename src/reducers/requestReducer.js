const requestReducer = (state = {loading: false, error: false}, action) => {
  switch (action.type) {
    case 'CRYPTO_FETCH_REQUESTED':
      return {
        ...state,
        ...( action.status )
      };
    case 'CRYPTO_FETCH_SUCCEEDED':
      return {
        ...state,
        ...( action.status )
      };
    case 'CRYPTO_FETCH_FAILED':
      return {
        ...state,
        ...( action.status )
      };
    default:
      return state

  }
};

export default requestReducer;