import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TableRow from "../components/TableRow";

const propTypes = {
  crypt: PropTypes.array.isRequired,

};

const defaultProps = {
  crypt: [ {}, {}, {} ]
};

class Table extends Component {

  render() {
    const {crypt} = this.props;
    // так как по условию просили отобразить 10-20, то slice
    // можно было при получении данных вытащить только данные из поля DefaultWatchlist, где находятся первые 10-из списка криптовалют
    const children = crypt.slice(0, 20).map((element, index) => {
      return <TableRow
          key={element.Id}
          number={index + 1}
          name={element.Name}
          ImageUrl={element.ImageUrl}
          BaseImageUrl={element.baseImgUrl}
      />
    });
    return (
        <table>
          <thead>
          <tr>
            <td> #</td>
            <td> Name</td>
            <td> Logo</td>
          </tr>
          </thead>
          <tbody>
          {children}
          </tbody>
        </table>
    )
  }
}


const mapStateToProps = (store) => ( {
  crypt: store.crypto
} );


Table.propTypes = propTypes;
Table.defaultProps = defaultProps;

export default connect(mapStateToProps)(Table);