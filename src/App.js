import React, { Component } from 'react';
import { connect } from 'react-redux';
import logo from './logo.svg';
import './App.css';
import { gettingCrypto } from "./actions/crypto";
import Table from "./containers/Table";
import Preloader from "./components/Preloader";

class App extends Component {
  render() {
    const {getCrypt, request} = this.props;
    return (
        <React.Fragment>
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo"/>
              <h1 className="App-title">Welcome to React</h1>
            </header>
            {request.loaded ? null : <button onClick={getCrypt}>Get Crypt</button>}
            <div className="table-container">
              {request.loaded ? <Table/> : null}

            </div>
          </div>
          {request.loading ? <Preloader/> : null}
        </React.Fragment>
    );
  }
}


const mapStateToProps = (store) => ( {
  request: store.request
} );

const mapDispatchToProps = (dispatch) => ( {
  getCrypt: () => dispatch(gettingCrypto())
} )

export default connect(mapStateToProps, mapDispatchToProps)(App);
